﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SlicerManager", menuName = "Config/Game/CarManager")]
public class CarController : ScriptableObject
{
    [SerializeField]  [Min(1)] public int openEachLvl = 1;
    [SerializeField] public PlayerCarsManagerData[] CarsOrder;

}
