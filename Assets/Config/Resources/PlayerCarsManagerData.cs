﻿using System;
using UnityEngine;

[Serializable] public class PlayerCarsManagerData
{
    [SerializeField] private Sprite sprite;
    [SerializeField] private Sprite outline;
    [SerializeField] private GameObject carGo;

    public GameObject CarGo => carGo;
    public Sprite Sprite => sprite;
    public Sprite Outline => outline;
}
