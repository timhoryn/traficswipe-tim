﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Config", order = 1)]
public class Config : ScriptableObject
{
    public GameObject EnptyGo;
    public int TrafficLengthMin = 1;
    public int TrafficLengthMax = 5;
    public int FreeZoneLengthMin = 1;
    public int FreeZoneLengthMax = 5;
    public int Line1Speed = 0;
    public int Line3Speed = 0;
    public float RotationSpeed;
    public int Speed;
    public GameObject[] tilePrefabs;
    public GameObject[] CarsPrefabs;
    public GameObject tileFinish;
    public int LevelLength = 50;
    public Curved[] Curvature;
}