﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutor : MonoBehaviour
{
    [SerializeField] private GameObject TutorGo;
    [SerializeField] private Animator Anim;
    [SerializeField] private Text txt;

    [SerializeField] private GameObject UsualTutorGo;
    [SerializeField] private Text Usualtxt;
    private void OnEnable()
    {

        EventManager.Subscribe(eEventType.Tutor, (arg) => TutorActive((float)arg));
        EventManager.Subscribe(eEventType.TutorStop, (arg) => TutorDisable());
        EventManager.Subscribe(eEventType.UsualTutor, (arg) => UsualTutorActive((float)arg));

    }

    private void TutorActive(float lineNow)
    {
        Time.timeScale = 0.0001f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        TutorGo.SetActive(true);
        Anim.SetFloat("swipe", lineNow - 2);
        if (lineNow == 3)
        {
            txt.text = "swipe left";
        }
        else
        {
            txt.text = "swipe right";
        }
    }
    private void UsualTutorActive(float lineNow)
    {
        if (FindObjectOfType<SceneControler>().GetConfigIndex() > 0)
        {
            UsualTutorGo.SetActive(true);
            if (lineNow == 3)
            {
                Usualtxt.text = "swipe left";
            }
            else
            {
                Usualtxt.text = "swipe right";
            }

        }
    }
    private void TutorDisable()
    {
        Time.timeScale = 1f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        TutorGo.SetActive(false);
        UsualTutorGo.SetActive(false);

    }

    private void OnDisable()
    {
        EventManager.Unsubscribe(eEventType.Tutor, (arg) => TutorActive((float)arg));
        EventManager.Unsubscribe(eEventType.TutorStop, (arg) => TutorDisable());
        EventManager.Unsubscribe(eEventType.UsualTutor, (arg) => UsualTutorActive((float)arg));
    }
}
