﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{


    private GameObject trafficPoint;
    private bool skip = false;

    private void OnEnable()
    {
        trafficPoint = FindObjectOfType<TrafficGenerator>().Line1GeneratePoint;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(typeof(CarControler), out Component component))
        {
            EventManager.OnEvent(eEventType.Win);
        }
    }
    private void FixedUpdate()
    {
        if(Vector3.Distance(transform.position, trafficPoint.transform.position)<30&&!skip)
        {
            skip = true;
            FindObjectOfType<TrafficGenerator>().enabled=false;
        }
    }
}
