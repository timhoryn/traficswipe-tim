﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PPSController : MonoBehaviour
{
    [SerializeField] private PostProcessVolume PPS;
    private ColorGrading CG;
    private Vignette VG;
    private void OnEnable()
    {
        PPS = GetComponent<PostProcessVolume>();
        PPS.profile.TryGetSettings(out CG);
        PPS.profile.TryGetSettings(out VG);
        EventManager.Subscribe(eEventType.TimeSpeed, (arg) => ChangeTimeSpeed((float)arg));

    }

    private void ChangeTimeSpeed(float timeSpeed)
    {
        CG.mixerRedOutGreenIn.value = (1f - timeSpeed) * 140;
        VG.intensity.value = (1f - timeSpeed) * 0.9f;

    }

    private void OnDisable()
    {
        EventManager.Unsubscribe(eEventType.TimeSpeed, (arg) => ChangeTimeSpeed((float)arg));
    }
}
