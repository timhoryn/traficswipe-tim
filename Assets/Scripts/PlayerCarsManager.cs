﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCarsManager : MonoBehaviour
{
    [SerializeField] private CarController CarConfig;
    private int CarId;
    private void OnEnable()
    {

        DontDestroyOnLoad(this.gameObject);

        if (PlayerPrefs.HasKey("CarId"))
        {
            CarId = PlayerPrefs.GetInt("CarId");
        }
        else
        {
            PlayerPrefs.SetInt("CarId", 0);
            CarId = 0;
        }
    }

    public int GetOpenEachLvl()
    {
        return CarConfig.openEachLvl;
    }
    public GameObject GetGo()
    {
        return CarConfig.CarsOrder[CarId].CarGo;
    }
    public Sprite GetNextSprite()
    {
        return CarConfig.CarsOrder[CarId + 1].Sprite;
    }
    public Sprite GetNextOutline()
    {
        if (CarId + 1 > CarConfig.CarsOrder.Length)
        {
            return CarConfig.CarsOrder[CarId + 1].Outline;

        }
        else
        {
            return CarConfig.CarsOrder[CarId].Outline;

        }


    }
    public void SetNextCar()
    {
        CarId++;
        if (CarId > CarConfig.CarsOrder.Length)
        {
            CarId = CarConfig.CarsOrder.Length;
        }
        PlayerPrefs.SetInt("CarId", CarId);

    }
}
