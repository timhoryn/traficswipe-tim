﻿using Ketchapp.MayoSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControler : MonoBehaviour
{
    [SerializeField] private Config[] Configs;
    private int configIndex = 0;


    private void OnEnable()
    {
        EventManager.Subscribe(eEventType.LevelRestart, (arg) => restart());
        EventManager.Subscribe(eEventType.LevelComplete, (arg) => nextLavel());

        DontDestroyOnLoad(this.gameObject);

        if (PlayerPrefs.HasKey("Level"))
        {
            configIndex = PlayerPrefs.GetInt("Level");
        }
        else
        {

            KetchappSDK.CrossPromo.ShowInterstitial();

            PlayerPrefs.SetInt("Level", 0);
            configIndex = 0;
        }
        SceneManager.LoadScene(1);
        Debug.Log("Level:" + configIndex);
    }

    public Config GetConfig()
    {
        return Configs[configIndex];
    }
    public int GetConfigIndex()
    {
        return configIndex;
    }
    private void nextLavel()
    {
        configIndex++;
        if (configIndex >= Configs.Length)
        {
            configIndex = 0;//Random.Range(0, Configs.Length);
        }
        PlayerPrefs.SetInt("Level", configIndex);
        SceneManager.LoadScene(1);
    }
    private void restart()
    {
        SceneManager.LoadScene(1);
    }
}
