﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficCar : MonoBehaviour
{
    private bool isMoving = true;
    private float speed = 1;

    private GameObject mesh;
    private Animator anim;
    private void OnEnable()
    {
        if (transform.childCount > 0)
        {
            mesh = transform.GetChild(0).gameObject;
            EventManager.Subscribe(eEventType.Lost, (arg) => disableAnim());
            anim = mesh.GetComponent<Animator>();
        }
        EventManager.Subscribe(eEventType.Win, (arg) => turnOnGravity());
        EventManager.Subscribe(eEventType.Lost, (arg) => turnOnGravity());


    }
    private void Update()
    {
        if (isMoving)
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
            transform.rotation =Quaternion.identity;
        }
    }

    private void disableAnim()
    {
        anim.enabled = false;
    }
    public void SetSpeed(float Speed)
    {
        speed = Speed;
        if (speed < 0 && mesh != null)
        {
            mesh.transform.Rotate(0, 180, 0);
        }
    }
    private void turnOnGravity()
    {
        isMoving = false;
        if (gameObject.TryGetComponent(typeof(Rigidbody), out Component component))
        {
            GetComponent<Rigidbody>().useGravity = true;
        }
    }

    private void OnDisable()
    {
        EventManager.Unsubscribe(eEventType.Lost, (arg) => disableAnim());

        EventManager.Unsubscribe(eEventType.Win, (arg) => turnOnGravity());
        EventManager.Unsubscribe(eEventType.Lost, (arg) => turnOnGravity());
    }
}
