﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficGenerator : MonoBehaviour
{
    public GameObject Line1GeneratePoint { get; private set; }
    public GameObject Line3GeneratePoint { get; private set; }

    private bool isWorking = false;
    private float distance = 6;

    private Config config;

    private bool line1TrafficZoneWrite = true;
    private int zone1LengtNow = 0;
    private int zone3LengtNow = 0;
    private int zone1Lengt = 2;
    private int zone3Lengt = 2;


    GameObject Line1PreviousGeneratedObject, Line3PreviousGeneratedObject;

    float LineSpeedDifference = 0;
    CarControler Player;
    private void OnEnable()
    {
        config = FindObjectOfType<SceneControler>().GetConfig();

        Line1GeneratePoint = transform.GetChild(0).gameObject;
        Line3GeneratePoint = transform.GetChild(1).gameObject;

        Line1PreviousGeneratedObject = GenerateEmpty(Line1GeneratePoint.transform.position, config.Line1Speed);
        Line3PreviousGeneratedObject = GenerateEmpty(Line3GeneratePoint.transform.position, config.Line3Speed);

        EventManager.Subscribe(eEventType.LevelStart, (arg) => setWork());
        EventManager.Subscribe(eEventType.Win, (arg) => setUnWork());
        EventManager.Subscribe(eEventType.Lost, (arg) => setUnWork());


        Player = FindObjectOfType<CarControler>();
        LineSpeedDifference = config.Line3Speed - config.Line1Speed;
 
        Line1GeneratePoint.transform.position = new Vector3(Line1GeneratePoint.transform.position.x, Line1GeneratePoint.transform.position.y, Line1GeneratePoint.transform.position.z + LineSpeedDifference * 7f / (config.Speed / 10f));

    }

    private void setWork()
    {
        isWorking = true;
    }
    private void setUnWork()
    {
        isWorking = false;
    }
    private void FixedUpdate()
    {
        if (Vector3.Distance(Line1PreviousGeneratedObject.transform.position, Line1GeneratePoint.transform.position) > distance && isWorking)
        {
            if (line1TrafficZoneWrite)
            {

                if (zone1LengtNow <= zone1Lengt)
                {
                    zone1LengtNow++;
                    Line1PreviousGeneratedObject = GenerateCar(Line1GeneratePoint.transform.position, config.Line1Speed);
                }
                else
                {

                    zone1LengtNow = 0;
                    zone1Lengt = Random.Range(config.FreeZoneLengthMin, config.FreeZoneLengthMax);
                    line1TrafficZoneWrite = false;
                }

            }

            if (!line1TrafficZoneWrite)
            {
                if (zone1LengtNow <= zone1Lengt)
                {
                    zone1LengtNow++;
                    Line1PreviousGeneratedObject = GenerateEmpty(Line1GeneratePoint.transform.position, config.Line1Speed);
                }
                else
                {

                    zone1LengtNow = 0;
                    zone1Lengt = Random.Range(config.TrafficLengthMin, config.TrafficLengthMax);
                    line1TrafficZoneWrite = true;
                }
            }
        }






        if (Vector3.Distance(Line3PreviousGeneratedObject.transform.position, Line3GeneratePoint.transform.position) > distance && isWorking)
        {
            if (!line1TrafficZoneWrite)
            {
                if (zone3LengtNow <= zone3Lengt)
                {
                    zone3LengtNow++;
                    Line3PreviousGeneratedObject = GenerateCar(Line3GeneratePoint.transform.position, config.Line3Speed);
                }
                else
                {

                    zone3LengtNow = 0;
                    zone3Lengt = Random.Range(config.FreeZoneLengthMin, config.FreeZoneLengthMax);
                    line1TrafficZoneWrite = false;
                }

            }

            if (line1TrafficZoneWrite)
            {
                if (zone3LengtNow <= zone3Lengt)
                {
                    zone3LengtNow++;
                    Line3PreviousGeneratedObject = GenerateEmpty(Line3GeneratePoint.transform.position, config.Line3Speed);
                }
                else
                {
                    zone3LengtNow = 0;
                    zone3Lengt = Random.Range(config.TrafficLengthMin, config.TrafficLengthMax);
                    line1TrafficZoneWrite = true;
                }
            }




        }

    }
    GameObject GenerateCar(Vector3 position, float speed)
    {
        GameObject TempCar = Instantiate(config.CarsPrefabs[Random.Range(0, config.CarsPrefabs.Length)], position, Quaternion.identity);
        TempCar.GetComponent<TrafficCar>().SetSpeed(speed);
        return TempCar;
    }

    GameObject GenerateEmpty(Vector3 position, float speed)
    {
        GameObject TempCar = Instantiate(config.EnptyGo, position, Quaternion.identity);
        TempCar.GetComponent<TrafficCar>().SetSpeed(speed);
        return TempCar;
    }

    private void OnDisable()
    {
        EventManager.Unsubscribe(eEventType.LevelStart, (arg) => setWork());
        EventManager.Unsubscribe(eEventType.Win, (arg) => setUnWork());
        EventManager.Unsubscribe(eEventType.Lost, (arg) => setUnWork());
    }
}
