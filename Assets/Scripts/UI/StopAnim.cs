﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopAnim : MonoBehaviour
{
    public void AnimStop()
    {
        EventManager.OnEvent(eEventType.StopWOWAnim);
    }
}
