﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class lvlText : MonoBehaviour
{
    private void OnEnable()
    {
        SceneControler sceneControler = FindObjectOfType<SceneControler>();
        GetComponent<Text>().text = "Level: " + (sceneControler.GetConfigIndex() + 1).ToString();
    }


}
