using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private eUIWindowType _defaultWindow;
    [SerializeField] private Image Ounline;
    [SerializeField] private Image FilledImg;
    [SerializeField] private Text FillText;

    private Dictionary<eUIWindowType, UIWindow> _windows = new Dictionary<eUIWindowType, UIWindow>();
    private UIWindow _currentWindow;

    private void OnEnable()
    {
        InitWIndows();
        ShowWindow(_defaultWindow);


        EventManager.Subscribe(eEventType.LevelStart, (arg) => ShowWindow(eUIWindowType.Game));
        EventManager.Subscribe(eEventType.Lost, (arg) => ShowWindow(eUIWindowType.Lose));
        EventManager.Subscribe(eEventType.Win, (arg) => win());

    }


    private void ShowWindow(eUIWindowType windowType)
    {
        if (_currentWindow != null) _currentWindow.Hide();
        if (_windows.TryGetValue(windowType, out _currentWindow))
        {
            _currentWindow.Show();
        }
    }

    private void InitWIndows()
    {
        foreach (Transform child in transform)
        {
            if (child.TryGetComponent(out UIWindow window))
            {
                _windows.Add(window.WindowType, window);
            }
        }
    }

    private void win()
    {
        ShowWindow(eUIWindowType.Win);
        PlayerCarsManager playerCarsManager = FindObjectOfType<PlayerCarsManager>();
        float fillfrom = (FindObjectOfType<SceneControler>().GetConfigIndex()) / (playerCarsManager.GetOpenEachLvl() / 100f);
        float fillto = (FindObjectOfType<SceneControler>().GetConfigIndex() + 1) / (playerCarsManager.GetOpenEachLvl() / 100f);

        if (fillfrom >= 100)
        {
            fillfrom = fillfrom % 100;
        }
        if (fillto > 100)
        {
            fillto = fillto % 100;
            if (fillto == 0)
            {
                fillto = 100;
            }
        }

        StartCoroutine(filling(fillfrom, fillto));


    }
    IEnumerator filling(float from, float to)
    {
        PlayerCarsManager playerCarsManager = FindObjectOfType<PlayerCarsManager>();
        Ounline.sprite = playerCarsManager.GetNextOutline();
        FilledImg.sprite = playerCarsManager.GetNextSprite();
        if (to >= 100)
        {
            playerCarsManager.SetNextCar();
        }

        for (float i = from; i <= to; i++)
        {
            FillText.text = Mathf.Round(i).ToString() + "%";

            FilledImg.fillAmount = i / 100;

            yield return new WaitForSeconds(0.03f);
        }
    }

    private void OnDisable()
    {
        EventManager.Unsubscribe(eEventType.LevelStart, (arg) => ShowWindow(eUIWindowType.Game));
        EventManager.Unsubscribe(eEventType.Lost, (arg) => ShowWindow(eUIWindowType.Lose));
        EventManager.Unsubscribe(eEventType.Win, (arg) => win());
    }

    //public void lvlBack()
    //{
    //    FindObjectOfType<SceneController>().BackLvl();
    //}
}

