using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartButton : BaseButton
{

    protected override void OnClick()
    {
        EventManager.OnEvent(eEventType.LevelRestart);
    }
}

