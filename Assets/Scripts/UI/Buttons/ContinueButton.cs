using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

    public class ContinueButton : BaseButton
    {
 
        protected override void OnClick()
        {
             Taptic.Light();
            EventManager.OnEvent(eEventType.LevelComplete);

        }
    }
