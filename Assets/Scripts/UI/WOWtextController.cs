﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WOWtextController : MonoBehaviour
{
    [SerializeField] private GameObject WOWparent;
    [SerializeField] private Text text;
    [SerializeField] private Animator animator;

    [SerializeField] private string[] Words;

    // Start is called before the first frame update
    private void OnEnable()
    {
         EventManager.Subscribe(eEventType.StopWOWAnim, (arg) => AnimStop());
        EventManager.Subscribe(eEventType.ExtremeJump, (arg) => ExtremeJump());
     }

    public void AnimStop()
    {
        WOWparent.SetActive(false);
    }
    private void ExtremeJump()
    {
        WOWparent.SetActive(true);
        text.text = Words[Random.Range(0, Words.Length)];
        animator.SetTrigger("Play");
    }
 
    private void OnDisable()
    {
        EventManager.Unsubscribe(eEventType.StopWOWAnim, (arg) => AnimStop());
        EventManager.Unsubscribe(eEventType.ExtremeJump, (arg) => ExtremeJump());
    }
}
