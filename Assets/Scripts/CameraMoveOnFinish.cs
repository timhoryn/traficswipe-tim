﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoveOnFinish : MonoBehaviour
{
    private bool finish = false;
    private Transform finishCamPos;
    private void OnEnable()
    {
        EventManager.Subscribe(eEventType.Win, (arg) => SetFinish());

    }
    private void SetFinish()
    {
        finishCamPos = FindObjectOfType<CarControler>().finishCamPos;

        finish = true;
        GetComponent<MoveWithPlayerZAxis>().enabled = false;
    }
    private void Update()
    {
        if (finish)
        {

            transform.position = Vector3.Lerp(transform.position, finishCamPos.position, 2 * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, finishCamPos.rotation, 2 * Time.deltaTime);
        }
    }


    private void OnDisable()
    {
        EventManager.Unsubscribe(eEventType.Win, (arg) => SetFinish());
    }
}
