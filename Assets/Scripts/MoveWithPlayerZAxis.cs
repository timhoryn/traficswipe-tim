﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWithPlayerZAxis : MonoBehaviour
{
    private float differenceZ = 0;
    private GameObject player;
    void OnEnable()
    {
        player = FindObjectOfType<CarControler>().gameObject;
        differenceZ = player.transform.position.z - transform.position.z;
    }

    private void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z - differenceZ);
    }
}
