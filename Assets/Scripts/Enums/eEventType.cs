
public enum eEventType
{
    LevelStart,
    LevelComplete,
    LevelRestart,
    Lost,
    Win,
    Landing,
    ExtremeJump,
    TutorStop,
    TimeSpeed,
    UsualTutor,
    Tutor,
    SwipeRight,
    SwipeLeft,
    StopWOWAnim
}

