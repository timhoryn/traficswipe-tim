﻿using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Curved
{
    public float HorizontalDistortion = 0;
    public float VerticalDistortion = 0;
}
