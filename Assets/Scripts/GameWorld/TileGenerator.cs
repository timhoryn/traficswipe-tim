﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileGenerator : MonoBehaviour
{
    private List<GameObject> activeTiles = new List<GameObject>();
    private GameObject[] tilePrefabs;
    private GameObject finish;

    public float tileLength { get; private set; } = 15;

    private float lvlLength = 0;
    private float spawnPos = 0;
    private int startTiles = 15;

    Transform player;

    // Start is called before the first frame update
    void OnEnable()
    {
        SceneControler sceneControler = FindObjectOfType<SceneControler>();
        tilePrefabs = sceneControler.GetConfig().tilePrefabs;
        lvlLength = sceneControler.GetConfig().LevelLength;
        finish = sceneControler.GetConfig().tileFinish;
        player = FindObjectOfType<CarControler>().transform;

        lvlLength -= startTiles;

        for (int i = 0; i < startTiles; i++)
        {
            SpawnTile(Random.Range(0, tilePrefabs.Length));
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (player.position.z - 100 > spawnPos - (startTiles * tileLength))
        {

            if (lvlLength != 0)
            {
                SpawnTile(Random.Range(0, tilePrefabs.Length));
                DeleteTile();
                lvlLength--;
            }
            else
            {
                GameObject nextTile = Instantiate(finish, transform.forward * spawnPos, transform.rotation, gameObject.transform);
                activeTiles.Add(nextTile);
                spawnPos += tileLength;
                lvlLength--;

            }
        }

    }

    private void SpawnTile(int tileIndex)
    {
        GameObject nextTile = Instantiate(tilePrefabs[tileIndex], transform.forward * spawnPos, transform.rotation, gameObject.transform);
        activeTiles.Add(nextTile);
        spawnPos += tileLength;
    }
    private void DeleteTile()
    {
        Destroy(activeTiles[0]);
        activeTiles.RemoveAt(0);
    }
}