﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bazier : MonoBehaviour
{
    public Transform p0 { get; private set; }
    public Transform p3 { get; private set; }

    private Transform p1;
    private Transform p2;
    private void OnEnable()
    {
        p0 = transform.GetChild(0);
        p1 = transform.GetChild(1);
        p2 = transform.GetChild(2);
        p3 = transform.GetChild(3);
    }
    public Vector3 GetPoint(float t)
    {
        Vector3 p01 = Vector3.Lerp(p0.position, p1.position, t);
        Vector3 p12 = Vector3.Lerp(p1.position, p2.position, t);
        Vector3 p23 = Vector3.Lerp(p2.position, p3.position, t);

        Vector3 p012 = Vector3.Lerp(p01, p12, t);
        Vector3 p123 = Vector3.Lerp(p12, p23, t);

        return Vector3.Lerp(p012, p123, t);
    }
}
