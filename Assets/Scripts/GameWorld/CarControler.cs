﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarControler : MonoBehaviour
{


    [SerializeField] private GameObject LostFX;
    [SerializeField] private GameObject RayGun;
    [SerializeField] private float RayDistance = 3;
    [SerializeField] private float RotationSpeed;
    [SerializeField] public int Speed;
    [SerializeField] public Transform finishCamPos { get; private set; }

    private bool isMoveLeft;
    private bool isMoveRight;
    private bool isFinish;
    private bool isWorking = false;
    private bool timeStop = false;
    private int lineNow = 3;
    private int tutorCounter = 0;

    private float arcProgress = 0;
    private float rotationForce = 20;

    private Vector3 PreviousVector = Vector3.zero;
    private GameObject mesh;
    private SceneControler sceneControler;
    private Rigidbody rb;
    private Bazier bazierArc;
    private void OnEnable()
    {
        Time.timeScale = 1;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;

        mesh = Instantiate(FindObjectOfType<PlayerCarsManager>().GetGo(), new Vector3(transform.position.x, transform.position.y - 0.6f, transform.position.z), Quaternion.Euler(0, -90, 0), transform);
        GetComponent<BoxCollider>().size = new Vector3(mesh.GetComponent<BoxCollider>().size.z, mesh.GetComponent<BoxCollider>().size.y, mesh.GetComponent<BoxCollider>().size.x);
        mesh.GetComponent<BoxCollider>().enabled = false;
        RayGun.transform.position = new Vector3(RayGun.transform.position.x, RayGun.transform.position.y, RayGun.transform.position.z + GetComponent<BoxCollider>().size.z / 2);
        sceneControler = FindObjectOfType<SceneControler>();
        bazierArc = FindObjectOfType<Bazier>();
        RotationSpeed = sceneControler.GetConfig().RotationSpeed;
        Speed = sceneControler.GetConfig().Speed;

        finishCamPos = transform.GetChild(3);
        rb = GetComponent<Rigidbody>();

        EventManager.Subscribe(eEventType.SwipeLeft, (arg) => moveLeft());
        EventManager.Subscribe(eEventType.SwipeRight, (arg) => moveRight());
        EventManager.Subscribe(eEventType.LevelStart, (arg) => activate());
        EventManager.Subscribe(eEventType.LevelStart, (arg) => gameStart());
        EventManager.Subscribe(eEventType.Win, (arg) => finish());
        EventManager.Subscribe(eEventType.Lost, (arg) => disable());
        EventManager.Subscribe(eEventType.Landing, (arg) => landingF());


        rb.centerOfMass = new Vector3(rb.centerOfMass.x, rb.centerOfMass.y - 0.3f, rb.centerOfMass.z);
    }
    private void gameStart()
    {
        EventManager.OnEvent(eEventType.UsualTutor, (float)lineNow);
    }
    private void finish()
    {
        isFinish = true;
        disable();
    }

    private void activate()
    {
        isWorking = true;
    }
    private void landingF()
    {
        timeStop = false;
        ParticleSystem[] hingeJoints = mesh.GetComponentsInChildren<ParticleSystem>();

        foreach (ParticleSystem joint in hingeJoints)
            joint.Play();


        StartCoroutine(returntoNormalSpeedTime());


    }
    private void disable()
    {
        EventManager.Unsubscribe(eEventType.SwipeLeft, (arg) => moveLeft());
        EventManager.Unsubscribe(eEventType.SwipeRight, (arg) => moveRight());
        EventManager.Unsubscribe(eEventType.LevelStart, (arg) => activate());
        EventManager.Unsubscribe(eEventType.LevelStart, (arg) => gameStart());
        EventManager.Unsubscribe(eEventType.Win, (arg) => finish());
        EventManager.Unsubscribe(eEventType.Lost, (arg) => disable());
        EventManager.Unsubscribe(eEventType.Landing, (arg) => landingF());

        isWorking = false;
    }

    private void FixedUpdate()
    {

        if (isWorking)
        {
            Vector3 vector = Vector3.zero;
            if (isMoveLeft)
            {

                arcProgress += RotationSpeed / 5 * Time.fixedDeltaTime;
                if (timeStop)
                {
                    if (arcProgress < 0.5f)
                    {
                        Time.timeScale = 1f - arcProgress;
                        Time.fixedDeltaTime = Time.timeScale * 0.02f;
                    }
                    EventManager.OnEvent(eEventType.TimeSpeed, Time.timeScale);
                }
                vector += (bazierArc.GetPoint(arcProgress) - transform.position) * rotationForce;
                transform.rotation = Quaternion.Euler(0, 0, 360 * arcProgress);
                if (arcProgress >= 1)
                {
                    EventManager.OnEvent(eEventType.Landing);

                    isMoveLeft = false;
                }
            }
            else if (isMoveRight)
            {
                arcProgress -= RotationSpeed / 5 * Time.fixedDeltaTime;
                if (timeStop)
                {
                    if (arcProgress > 0.5f)
                    {
                        Time.timeScale = arcProgress;
                        Time.fixedDeltaTime = Time.timeScale * 0.02f;
                    }
                    EventManager.OnEvent(eEventType.TimeSpeed, Time.timeScale);

                }
                vector += (bazierArc.GetPoint(arcProgress) - transform.position) * rotationForce;
                transform.rotation = Quaternion.Euler(0, 0, 360 * arcProgress);
                if (arcProgress <= 0)
                {
                    EventManager.OnEvent(eEventType.Landing);

                    isMoveRight = false;
                }
            }
            else
            {
                if (timeStop)
                {
                    Time.timeScale += 0.01f;
                    Time.fixedDeltaTime = Time.timeScale * 0.02f;
                    if (Time.timeScale >= 1)
                    {
                        timeStop = false;
                    }
                }
                Vector3 dir = Vector3.zero;
                if (lineNow == 1)
                {
                    dir = new Vector3(bazierArc.p3.position.x, bazierArc.p3.position.y, bazierArc.p3.position.z + 2) - transform.position;
                }
                else if (lineNow == 3)
                {
                    dir = new Vector3(bazierArc.p0.position.x, bazierArc.p0.position.y, bazierArc.p0.position.z + 2) - transform.position;
                }
                Quaternion rotanior = Quaternion.LookRotation(dir);
                transform.rotation = Quaternion.Lerp(transform.rotation, rotanior, 1 * Time.fixedDeltaTime);

                PreviousVector = vector;
            }
            rb.velocity = rb.velocity - PreviousVector + (vector);
            rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, (transform.forward * Speed).z);
            PreviousVector = vector;
        }
        else
        {
            if (rb.velocity.z < 0.01f)
            {
                rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, 0);
            }
            else if (isFinish)
            {
                rb.velocity /= 1.01f;

                if (lineNow == 1)
                {
                    transform.Rotate(Vector3.down / 4);
                }
                else
                {
                    transform.Rotate(Vector3.up / 4);
                }
            }


        }
        Ray ray = new Ray(RayGun.transform.position, Vector3.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, RayDistance) && tutorCounter > 0)
        {
            if (!timeStop)
            {
                EventManager.OnEvent(eEventType.ExtremeJump);
            }
            timeStop = true;

        }

        if (Physics.Raycast(ray, out hit, 5) && (arcProgress <= 0 || arcProgress >= 1))
        {
            if (sceneControler.GetConfigIndex() == 0 && tutorCounter < 2)
            {
                EventManager.OnEvent(eEventType.Tutor, (float)lineNow);

            }

        }

    }
    IEnumerator returntoNormalSpeedTime()
    {
        while (Time.timeScale <= 1)
        {

            Time.timeScale += 0.02f;
            Time.fixedDeltaTime = Time.timeScale * 0.02f;
            EventManager.OnEvent(eEventType.TimeSpeed, Time.timeScale);
            yield return new WaitForSecondsRealtime(0.08f);
        }
    }

    private void moveLeft()
    {
        EventManager.OnEvent(eEventType.TutorStop);
        if (lineNow != 1 && !isMoveRight && !isMoveRight)
        {
            tutorCounter++;
            isMoveLeft = true;
            lineNow = 1;
        }
    }
    private void moveRight()
    {
        EventManager.OnEvent(eEventType.TutorStop);
        if (lineNow != 3 && !isMoveRight && !isMoveRight)
        {
            tutorCounter++;
            isMoveRight = true;
            lineNow = 3;
        }

    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent(typeof(TrafficCar), out Component component))
        {
            EventManager.OnEvent(eEventType.Lost);
            LostFX.transform.position = collision.contacts[0].point;
            LostFX.SetActive(true);

        }
    }

    private void OnDisable()
    {
        disable();
    }

}

