﻿using Ketchapp.MayoSDK;
using Ketchapp.MayoSDK.Analytics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Analytics : MonoBehaviour
{
    private ILevel level;
    private SceneControler sceneControler;
     private void OnEnable()
    {
        EventManager.Subscribe(eEventType.LevelStart, (arg) => level.ProgressionStart());
        EventManager.Subscribe(eEventType.Win, (arg) => level.ProgressionComplete());
        EventManager.Subscribe(eEventType.Lost, (arg) => level.ProgressionFailed());
 
        sceneControler = FindObjectOfType<SceneControler>();
        level = KetchappSDK.Analytics.GetLevel(sceneControler.GetConfigIndex() + 1);
    }

    private void OnDisable()
    {
        EventManager.Unsubscribe(eEventType.LevelStart, (arg) => level.ProgressionStart());
        EventManager.Unsubscribe(eEventType.Win, (arg) => level.ProgressionComplete());
        EventManager.Unsubscribe(eEventType.Lost, (arg) => level.ProgressionFailed());
    }
}
