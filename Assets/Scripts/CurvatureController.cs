﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurvatureController : MonoBehaviour
{

    private Curved[] Curvature;
    private int index = 1;
    private int levelLength;
    private float tileLength;
    private float speed;
    private bool moveUp = false;
    private bool moveDown = false;
    private bool IsWorking = true;
    private GameObject Player;
    private SceneControler sceneControler;

    // Start is called before the first frame update
    private void OnEnable()
    {
        EventManager.Subscribe(eEventType.Win, (arg) => disable());
        EventManager.Subscribe(eEventType.Lost, (arg) => disable());

        sceneControler = FindObjectOfType<SceneControler>();
        levelLength = sceneControler.GetConfig().LevelLength;
        speed = sceneControler.GetConfig().Speed * 2;
        tileLength = FindObjectOfType<TileGenerator>().tileLength;
        Curvature = sceneControler.GetConfig().Curvature;
        FindObjectOfType<AmazingAssets.CurvedWorld.CurvedWorldController>().bendHorizontalSize = sceneControler.GetConfig().Curvature[0].HorizontalDistortion;
        FindObjectOfType<AmazingAssets.CurvedWorld.CurvedWorldController>().bendVerticalSize = sceneControler.GetConfig().Curvature[0].VerticalDistortion;
        Player = FindObjectOfType<CarControler>().gameObject;
    }

    private void disable()
    {
        IsWorking = false;
    }
    // Update is called once per frame
    private void Update()
    {
        if (IsWorking)
        {
            if (moveUp)
            {
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
            }
            else if (moveDown)
            {
                transform.Translate(Vector3.back * (speed / 2) * Time.deltaTime);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, Player.transform.position.z);
            }
        }
    }
    private void FixedUpdate()
    {
        if (IsWorking)
        {
            if (Player.transform.position.z > transform.position.z)
            {


                moveUp = false;
                moveDown = false;
            }
            if (transform.position.z > (levelLength / Curvature.Length * tileLength * index) && !moveDown)
            {


                moveUp = true;
            }
            if (Player.transform.position.z + tileLength * 6 < transform.position.z && !moveDown)
            {

                moveUp = false;
                moveDown = true;
                index++;
                try
                {
                    FindObjectOfType<AmazingAssets.CurvedWorld.CurvedWorldController>().bendHorizontalSize = FindObjectOfType<SceneControler>().GetConfig().Curvature[index - 1].HorizontalDistortion;
                    FindObjectOfType<AmazingAssets.CurvedWorld.CurvedWorldController>().bendVerticalSize = FindObjectOfType<SceneControler>().GetConfig().Curvature[index - 1].VerticalDistortion;
                }
                catch
                { }
            }
        }

    }
    private void OnDisable()
    {
        EventManager.Unsubscribe(eEventType.Win, (arg) => disable());
        EventManager.Unsubscribe(eEventType.Lost, (arg) => disable());

    }
}
