﻿// Cristian Pop - https://boxophobic.com/

using UnityEngine;

namespace Boxophobic.Utils
{
    [CreateAssetMenu(fileName = "Data", menuName = "BOXOPHOBIC/Settings Data")]
    public class SettingsData : ScriptableObject
    {
        [Space]
        public string data = "";
        /*
         *  public float RotationSpeed;
        public int Speed;
        public GameObject[] CarsPrefabs;
        public int TrafficLengthMin = 1;
        public int TrafficLengthMax = 5;
        public int FreeZoneLengthMin = 1;
        public int FreeZoneLengthMax = 5;
         */
    }
}