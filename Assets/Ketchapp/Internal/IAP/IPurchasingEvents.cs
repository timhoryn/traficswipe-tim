﻿using System;
using Ketchapp.MayoSDK.Purchasing;

namespace Ketchapp.MayoSDK.Purchasing
{
    public interface IPurchasingEvents
    {
#if UNITY_PURCHASING
        private void OnPurchaseRestoreResult(PurchaseProductResult restoredProduct);
#endif
    }
}
