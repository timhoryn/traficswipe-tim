﻿using UnityEngine;
using UnityEngine.UI;

public class TapticControler : MonoBehaviour
{

    private void Start()
    {
        Taptic.tapticOn = true;
        EventManager.Subscribe(eEventType.Win, (arg) => success());
        EventManager.Subscribe(eEventType.Lost, (arg) => medium());
        EventManager.Subscribe(eEventType.Landing, (arg) => light());

    }

    private void success()
    {
        
        Taptic.Success();
    }
    private void medium()
    {
       
        Taptic.Medium();
    }
    private void light()
    {
        Taptic.Light();
    }

    private void OnDisable()
    {
        EventManager.Unsubscribe(eEventType.Win, (arg) => success());
        EventManager.Unsubscribe(eEventType.Lost, (arg) => medium());
        EventManager.Unsubscribe(eEventType.Landing, (arg) => light());
    }


}